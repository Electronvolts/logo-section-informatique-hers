# Logo section informatique HERS

## Name
Logo section informatique HERS

## Description
Logo de section du Bachelier informatique de la Haute Ecole Robert Schuman, implantation de Libramont.

## Authors and acknowledgment
Haute Ecole Robert Schuman (HERS).

## License
<WORK> (c) by Haute Ecole Robert Schuman (HERS)<AUTHOR(S)>

<WORK> is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see http://creativecommons.org/licenses/by-sa/4.0/.
